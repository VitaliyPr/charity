import * as flsFunctions from "./modules/functions.js";

flsFunctions.isWebp();

flsFunctions.pageWidth();


// Меню
$(".openbtn").click(function () {
  if ($(window).width() < 767.98) {
    document.getElementById('menu').style.width = '100%';
  } else {
    document.getElementById('menu').style.width = '100vw';
  };
});

$(".closebtn").click(function () {
  document.getElementById('menu').style.width = '0';
});


if ($(window).width() > 992) {
  $(window).scroll(function () {
    var height = $(window).scrollTop();
  
    if (height > 100) {
      $('header').addClass('header-fixed');
    } else {
      $('header').removeClass('header-fixed');
    }
  });

  $('#plans-item1').on('click', function () {
    $(this).toggleClass('active');
    $('#plans-item2').toggleClass('hide');
    $('#plans-item3').toggleClass('hide move1');
    $('#plans-item4').toggleClass('hide move1');
  });
  
  $('#plans-item2').on('click', function () {
    $(this).toggleClass('active');
    $('#plans-item1').toggleClass('hide');
    $('#plans-item3').toggleClass('hide move2');
    $('#plans-item4').toggleClass('hide move2');
  });
  
  $('#plans-item3').on('click', function () {
    $(this).toggleClass('active');
    $('#plans-item1').toggleClass('hide move3');
    $('#plans-item2').toggleClass('hide move3');
    $('#plans-item4').toggleClass('hide');
  });
  
  $('#plans-item4').on('click', function () {
    $(this).toggleClass('active');
    $('#plans-item1').toggleClass('hide move4');
    $('#plans-item2').toggleClass('hide move4');
    $('#plans-item3').toggleClass('hide');
  });
  
  $('#support-item1').on('click', function () {
    $(this).toggleClass('active');
    $('.support__head').toggleClass('hide');
    $('#support-item2').toggleClass('hide');
    $('#support-item3').toggleClass('hide');
  });
  
  $('#support-item2').on('click', function () {
    $(this).toggleClass('active');
    $('.support__head').toggleClass('hide');
    $('#support-item1').toggleClass('hide');
    $('#support-item3').toggleClass('hide');
  });
  
  $('#support-item3').on('click', function () {
    $(this).toggleClass('active');
    $('.support__head').toggleClass('hide');
    $('#support-item1').toggleClass('hide');
    $('#support-item2').toggleClass('hide');
  });
}

if ($('.accordion').length) {
  $('.accordion').accordion({
    onChanging: function onChanging() {
      $(this).closest('.accordion').toggleClass('is-active');
    }
  });
  $('[data-accordion-close]').on('click', function (evt) {
    evt.preventDefault();
    $('.accordion').accordion('close', 0);
  });
};



import Swiper from 'swiper/bundle';

if (document.querySelector('[data-charity]')) {
  const rest_sl = new Swiper('[data-charity]', {
    observer: true,
    observeParents: true,
    slidesPerView: 1,
    spaceBetween: 0,
    loop: true,
    autoHeight: true,
    speed: 800,
    pagination: {
      el: '[data-charity] .sl-nav__scroll',
      type: 'progressbar',
    },
    navigation: {
      nextEl: '[data-charity] .sl-nav__next',
      prevEl: '[data-charity] .sl-nav__prev',
    },
    breakpoints: {
      992: {
        autoHeight: false,
      }
    }
  });

  rest_sl.on('transitionStart', function () {
    var currentSlide = document.querySelector('[data-charity] .sl-nav__current');
    var current = rest_sl.realIndex + 1;
    var idx = current < 10 ? ("0" + current) : current;
    currentSlide.innerHTML = idx;
  });
};